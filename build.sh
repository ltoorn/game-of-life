if [ ! -d target ]
then
	mkdir target
fi


if [ $# == 0 ]
then
	echo -e "Options:
r	-	release build;
d	-	debug build;
clean	-	clean out target."

elif [ $1 == "d" ]
then
	clang -g -std=c99 -pedantic-errors -Wall -DDEBUG=1 -o target/game_of_life-debug src/main.c -lxcb

elif [ $1 == "r" ]
then
	clang -O1 -std=c99 -o target/game_of_life src/main.c -lxcb

elif [ $1 == "clean" ]
then
	rm target/*

else
	echo "Ignoring unknown option: $1"
fi
