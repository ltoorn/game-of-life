/* 
 MIT License
 
 Copyright © 2020 Lucas Christiaan van den Toorn

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the Software), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
*/

// The rules, from the wikipedia page for conway's game of life:
//
// 1. Any live cell with fewer than two live neighbors dies, as if by underpopulation.
// 2. Any live cell with two or three live neighbors lives on to the next generation.
// 3. Any live cell with more than three live neighbors dies, as if by overpopulation.
// 4. Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

// For the declaration of nanosleep:
#define __USE_POSIX199309
#include <time.h>

#ifdef __linux__
#include <unistd.h>
#include <xcb/xcb.h>
#endif

#if DEBUG

#define Assert(_pred)   if (!(_pred)) { \
                            fprintf(stderr, "Assertion failed at line: %d, of file: %s:\n" #_pred "\n", __LINE__, __FILE__); \
                            *(volatile int *)0x0 = 0; \
                        }

#define DebugPrintLnArgs(_msg, ...) printf("DEBUG :: " _msg "\n", __VA_ARGS__)
#define DebugPrintLnNoArgs(_msg)    printf("DEBUG :: " _msg "\n")

#else

#define Assert(pred)
#define DebugPrintLnArgs(_msg, ...)
#define DebugPrintLnNoArgs(_msg)

#endif // DEBUG

// Per X window protocol definition all function keys have zeros in the first and
// second bytes, ones in the third byte. The fourth byte actually discerns between
// the different function keys.
#define IsFunctionKey(_k) ((_k)&0xff00 && !((_k)&0xffff0000))

// @note(ltoorn): This will evaluate _n twice if it is an expression that cannot be evaluated at compile time.
// 
#define ClampNegativeToZero(_n) ((_n) < 0 ? 0 : (_n))

#define IsPixelCoordinateInBounds(_grid, _cdt) ((_cdt.x < _grid.nr_of_cells_per_row*SQUARE_SIZE_OF_CELL_IN_PIXELS) && _cdt.y < (_grid.nr_of_cells_per_column*SQUARE_SIZE_OF_CELL_IN_PIXELS))

#define Bool int

// @note(ltoorn): The number of bits per pixel is defined as the depth of the screen, which we
// inherit from `screen->root_depth`. For now we assume that it's always going to be 24. I couldn't
// find anything about per pixel padding in the documentation, but trial and error showed that
// `xcb_put_image()` did expect a padding byte at the end of every pixel. That means that the 
// format (for full color ZFormat pixmaps) is as follows:
//
// 24 (meaningful) bits per pixel, so 3 bytes: RGB. But layed out in memory according to a little
// endian format: BGR. Plus a padding byte on the end, resulting in BGRx.
#define BYTES_PER_PIXEL 4

// @todo(ltoorn): Make this into a user specifiable constant
#define SQUARE_SIZE_OF_CELL_IN_PIXELS 10

#define MAX_NR_OF_NEIGHBORS 8

#define TOP_LEFT_NEIGHBOR   0
#define TOP_MIDDLE_NEIGHBOR 1
#define TOP_RIGHT_NEIGHBOR  2
#define LEFT_NEIGHBOR       3
#define RIGHT_NEIGHBOR      4
#define BOTTOM_LEFT_NEIGHBOR   5
#define BOTTOM_MIDDLE_NEIGHBOR 6
#define BOTTOM_RIGHT_NEIGHBOR  7

#define UP_ARROW_KEYSYM 0xff52
#define DOWN_ARROW_KEYSYM 0xff54

#define LOWER_BOUND_TIME_SPEC_NANO 0l
#define UPPER_BOUND_TIME_SPEC_NANO 950000000l
#define LOWER_BOUND_TIME_SPEC_SEC  0
#define UPPER_BOUND_TIME_SPEC_SEC  3
#define TIME_SPEC_STEP_SIZE_NANO   50000000l
#define TIME_SPEC_INITIAL_SIZE_NANO 200000000l
#define TIME_SPEC_STEP_SIZE_SEC    1
#define TIME_SPEC_INITIAL_SIZE_SEC 0

#define GAME_STATE_PAUSED 1
#define GAME_STATE_RUNNING (1<<1)

typedef struct gol_window_dimensions gol_window_dimensions;
typedef struct gol_window_hdr gol_window_hdr;
typedef struct keysyms_per_keycode keysyms_per_keycode;

typedef struct cell cell;
typedef struct grid grid;
typedef struct color color;
typedef struct coordinate2 coordinate2;

struct gol_window_dimensions {
    uint16_t width;
    uint16_t height;
};

struct gol_window_hdr {
    xcb_window_t      window_id;           // alias uint32_t   
    xcb_gcontext_t    graphics_context_id; // alias uint32_t
    gol_window_dimensions root_dimensions;
    xcb_connection_t *connection;
    xcb_screen_t     *screen;
    xcb_setup_t      *setup;
    uint8_t          *image_buffer;
};

struct keysyms_per_keycode {
    xcb_keysym_t sym0;
    xcb_keysym_t sym1;
    xcb_keysym_t sym2;
    xcb_keysym_t sym3;
};

struct cell {
    uint32_t state;
    uint16_t x_position;
    uint16_t y_position;
    uint32_t nr_of_live_neighbors;
    cell *neighbors[MAX_NR_OF_NEIGHBORS];
};

struct grid {
    size_t nr_of_cells_per_row;
    size_t nr_of_cells_per_column;
    size_t total_nr_of_cells;
    
    size_t max_nr_of_cells_per_row;
    size_t max_total_nr_of_cells;
   
    cell *cells;
   
    uint32_t game_state;
};

struct color {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
    uint8_t padding;
};

struct coordinate2 {
    int16_t x;
    int16_t y;
};

enum cell_flags {
    IS_ALIVE           = 1<<0,

    IS_IN_TOP_ROW      = 1<<1,
    IS_IN_BOTTOM_ROW   = 1<<2,
    IS_IN_LEFT_COLUMN  = 1<<3,
    IS_IN_RIGHT_COLUMN = 1<<4
};

// From the X protocol documentation: Keycodes lie in the inclusive range [8,255].
// See: https://www.x.org/releases/X11R7.7/doc/xproto/x11protocol.html#Keyboards
//
// We waste the first seven positions, so that we can use the keycodes directly as
// array indices, without having to subtract 8 every time we need to look up a 
// keycode (which is going to be very often indeed).
static uint32_t keycode_to_keysyms_table[255+1][4];

static color dead_cell_color;
static color live_cell_color;
static color border_color;

// @improve(ltoorn): proper error handling.
static void handle_fatal_error(char *error_msg) {
    // @hardening(ltoorn): make sure the error message printing is robust.
    fprintf(stderr, "ERROR: %s\n", error_msg);
    exit(1);
}

// X Window routines
static uint8_t *allocate_image_buffer(gol_window_dimensions root_dimensions) {

    // We need width*height*bytes_per_pixel amount of bytes in the image.
    size_t w   = (size_t) root_dimensions.width;
    size_t h   = (size_t) root_dimensions.height;
    size_t bpp = (size_t) BYTES_PER_PIXEL;

    uint8_t *image = (uint8_t *) malloc(w * h * bpp);
    // @incomplete(ltoorn): zero out memory (memset)

    if (!image) {
        // If we can't allocate a buffer to draw to, we cannot run.
        handle_fatal_error("unable to allocate sufficient memory for image.");
    }

    return image;
}

static gol_window_hdr initialize_window() {

    int screen_number;

    xcb_connection_t *connection = xcb_connect(0, &screen_number);

    if (!connection || xcb_connection_has_error(connection)) {
        handle_fatal_error("Failed to establish connection.");
    }

    // @incomplete(ltoorn): read out and use setup->image_byte_order (0 for little, 1 for big endian).
    //
    // For now we just assume we're always running on little endian, since it's by far the most
    // common. But for completeness sake, we should be able to support big endian as well, maybe
    // ... idunno. 
    const xcb_setup_t *setup = xcb_get_setup(connection);
    xcb_screen_iterator_t screen_iterator = xcb_setup_roots_iterator(setup);

    Bool found_screen = 0;
    for (int i = 0; screen_iterator.rem; ++i) {
        if (i == screen_number) {
            found_screen = 1;
            break;
        } else {
            xcb_screen_next(&screen_iterator);
        }
    }

    if (!found_screen) {
        handle_fatal_error("Unable to find screen");
    }

    xcb_screen_t *screen = screen_iterator.data;

    // @note(ltoorn): In the X window system a window is characterized by
    // an id. The `xcb_window_t` is a type alis for `uint32_t`.
    //
    // Before we can create a window, we must first get an id for it.
    xcb_window_t window_id = xcb_generate_id(connection);

    gol_window_dimensions root_dimensions = { screen->width_in_pixels, screen->height_in_pixels };
 
    // @note(ltoorn): The masks we can pass to the create_window function are defined in `xcb_cw_t`. 

    // We pass the event mask to be able to register for event types.
    // Note thate the order in which the values for the masks stored in the value list must be the same
    // as the order of the masks in the XCB_CW enumeration.
    // XCB_CW_BACK_PIXEL == 2, XCB_CW_EVENT_MASK == 2048
    uint32_t w_value_mask = XCB_CW_EVENT_MASK | XCB_CW_BACK_PIXEL;

    uint32_t w_value_list[] = { 0x000000, XCB_EVENT_MASK_EXPOSURE | XCB_EVENT_MASK_KEY_PRESS | XCB_EVENT_MASK_BUTTON_PRESS | XCB_EVENT_MASK_BUTTON_RELEASE };

    xcb_create_window(
            connection,
            screen->root_depth,
            window_id,
            screen->root,   // The id of the parent window.
            0,              // x
            0,              // y
            root_dimensions.width,
            root_dimensions.height,
            1,              // border width
            XCB_WINDOW_CLASS_INPUT_OUTPUT,
            screen->root_visual,
            w_value_mask,
            w_value_list);

    // @reference(ltoorn): The `format` parameter in the `xcb_change_property` function refers
    // to the memory layout of the individual elements of the data that is passed to the function
    // (final argument). X needs this information to correctly process this data (possible performing
    // byte swaps if necessary). The possible options are `8`, `16`, or `32` (for 8, 16, or 32 bit 
    // respectively).
    //
    // See: https://www.systutorials.com/docs/linux/man/3-xcb_change_property/

    // We're going to set the window title as well.
    char window_title[] = "Game of Life";
    xcb_change_property(
            connection,
            XCB_PROP_MODE_REPLACE,
            window_id,
            XCB_ATOM_WM_NAME,
            XCB_ATOM_STRING,
            8, // format @note(ltoorn): We're assuming an 8 bit per byte architecture, which is reasonable for a desktop computer.
            sizeof(window_title),
            (void *) window_title);

    // @incomplete(ltoorn): We should set the window icon as well.

    xcb_map_window(connection, window_id);

    xcb_flush(connection);

    // @todo(ltoorn): Check whether the gc we create is entirely appropriate.
    // In order to draw something to our screen we need to create a graphics context.
    // But before we can create a graphics context we first need to generate an identifier
    // to assign to it. (`xcb_gcontext_t` is itself just an integer).
    xcb_gcontext_t graphics_context_id = xcb_generate_id(connection);

    // We also need to pass some attributes to the graphics context. The attributes are 
    // defined as an enumeration and can be or'ed together. The corresponding values need
    // to be passed, _in the same order as they are defined_, as a simple array.
    uint32_t gc_value_mask   = XCB_GC_BACKGROUND;
    uint32_t gc_value_list[] = { screen->black_pixel };

    // Now that we have our identifier and attributes, we can actually create a graphics context.
    xcb_create_gc(
            connection,
            graphics_context_id,
            screen->root,
            gc_value_mask,
            gc_value_list);

    uint8_t *image_buffer = allocate_image_buffer(root_dimensions);

    gol_window_hdr header;
    header.window_id           = window_id;
    header.graphics_context_id = graphics_context_id;
    header.root_dimensions     = root_dimensions;
    header.connection          = connection;
    header.screen              = screen;
    header.setup               = (xcb_setup_t *) setup;
    header.image_buffer        = image_buffer;

    return header;
}

static void initialize_keycode_to_keysyms_lookup_table(gol_window_hdr hdr) {

    xcb_generic_error_t *err = 0;

    // We know the keycodes lie in the range 8, 255 (inclusive), but we take the min and max
    // keycodes from the setup struct anyway. Since we don't know if there's ever a case 
    // where the actual keycodes don't cover this full range.
    xcb_keycode_t min_keycode = hdr.setup->min_keycode;
    xcb_keycode_t max_keycode = hdr.setup->max_keycode;

    DebugPrintLnArgs("min_keycode: %d", min_keycode);
    DebugPrintLnArgs("max_keycode: %d", max_keycode);

    // We pass the min_keycode to `xcb_get_keyboard_mapping` as the `first_keycode` (second arg).
    // The X protocol documentation states that the `first_keycode` + (`count` - 1) must be <= max_keycode.
    //
    // See: https://www.x.org/releases/X11R7.7/doc/xproto/x11protocol.html#requests:GetKeyboardMapping
    uint8_t count = (max_keycode - min_keycode) + 1;

    xcb_get_keyboard_mapping_cookie_t cookie = xcb_get_keyboard_mapping(hdr.connection, min_keycode, count);
    
    xcb_get_keyboard_mapping_reply_t *reply = xcb_get_keyboard_mapping_reply(hdr.connection, cookie, &err);
    xcb_keysym_t *keysyms                   = xcb_get_keyboard_mapping_keysyms(reply);

    uint8_t keysyms_per_keycode = reply->keysyms_per_keycode; 

    if (err) {
        // @todo(ltoorn): handle error properly
        handle_fatal_error("error while retrieving keysyms for keycodes\n");
    }

    // Clear the lookup table to zero. 
    //
    // Since we only do this once at startup and the array is pretty small, we
    // just zero it in the most straightforward way.
    for (int i = 0; i < (255+1); ++i) {
        for (int j = 0; j < 4; ++j) {
            keycode_to_keysyms_table[i][j] = 0;
        }
    }

    for (int i = 0; i < count; ++i) {
        for (int j = 0; j < keysyms_per_keycode; ++j) {
            // We're really only interested in the first 4 keysyms. The keysyms_per_keycode value
            // is 'chosen arbitrarily' by the X server such that it is big enough to hold all keysyms
            // for any of the keycodes for which we've requested the mapping. The documentation on 
            // keyboard mappings (see: https://www.x.org/releases/X11R7.7/doc/xproto/x11protocol.html#Keyboards)
            // however, makes it pretty clear that we never need to consider more than 4 keysyms.
            if (j < 4) {
                keycode_to_keysyms_table[i+8][j] = *keysyms;
            }
            ++keysyms;
        }
    }
}

static xcb_keysym_t translate_keycode_to_keysym(xcb_keycode_t keycode) {
    xcb_keysym_t keysym = keycode_to_keysyms_table[keycode][0]; // @incomplete(ltoorn): we shouldn't ignore the other keysyms
    return keysym;
}
// End X Window routines

static inline void initialize_standard_colors(void) {
    dead_cell_color.red   = 0x60;
    dead_cell_color.green = 0x60;
    dead_cell_color.blue  = 0x9c;
    dead_cell_color.padding = 0x0;

    live_cell_color.red   = 0x1c;
    live_cell_color.green = 0x6a;
    live_cell_color.blue  = 0x1c;
    live_cell_color.padding = 0x0;

    border_color.red   = 0x4a;
    border_color.green = 0x4a;
    border_color.blue  = 0x89;
    border_color.padding = 0x0;
}

static inline size_t pixels_to_nr_of_cells(size_t pixels) {
    return (pixels/SQUARE_SIZE_OF_CELL_IN_PIXELS);
}

static grid initialize_grid(gol_window_dimensions root_dimensions) {

    // We need (width/SQUARE_SIZE_OF_CELL_IN_PIXELS) * (height/SQUARE_SIZE_OF_CELL_IN_PIXELS)
    size_t cells_per_row     = pixels_to_nr_of_cells((size_t)root_dimensions.width);
    size_t cells_per_column  = pixels_to_nr_of_cells((size_t)root_dimensions.height);
    size_t total_nr_of_cells = cells_per_row * cells_per_column;

    cell *cells = (cell *) malloc(total_nr_of_cells*sizeof(cell));

    if (!cells) {
        handle_fatal_error("unable to allocate sufficient memory for cells grid.");
    }

    for (int i = 0; i < total_nr_of_cells; ++i) {
        cells[i].state = 0;
        cells[i].x_position = 0;
        cells[i].y_position = 0;
        cells[i].nr_of_live_neighbors = 0;
        for (int j = 0; j < MAX_NR_OF_NEIGHBORS; ++j) {
            cells[i].neighbors[j] = 0;
        }
    }

    grid grid;

    grid.nr_of_cells_per_row     = cells_per_row;
    grid.nr_of_cells_per_column  = cells_per_column;
    grid.total_nr_of_cells       = total_nr_of_cells;
    grid.max_nr_of_cells_per_row = cells_per_row;
    grid.max_total_nr_of_cells   = total_nr_of_cells;
    grid.cells                   = cells;
    grid.game_state              = 0;

    return grid;
}

static void adjust_visible_grid_size_to_window_dimensions(grid *grid, gol_window_dimensions dimensions) {

    size_t previous_total_nr_of_cells = grid->total_nr_of_cells;

    size_t cells_per_row    = pixels_to_nr_of_cells((size_t)dimensions.width);
    size_t cells_per_column = pixels_to_nr_of_cells((size_t)dimensions.height);

    grid->nr_of_cells_per_row    = cells_per_row;
    grid->nr_of_cells_per_column = cells_per_column;
    grid->total_nr_of_cells      = cells_per_row*cells_per_column;

    Assert(grid->nr_of_cells_per_row <= grid->max_nr_of_cells_per_row);
    Assert(grid->total_nr_of_cells <= grid->max_total_nr_of_cells);

    // We first create a temporary buffer to hold pointers to cells that need to be alive after adjusting the
    // grid to the new dimensions. We calculate which cells need to be alive based on the x and y positions of
    // the cells that are currently alive in the old dimensions.
    cell *temp_live_cell_buffer[grid->total_nr_of_cells];
    for (size_t i = 0; i < grid->total_nr_of_cells; ++i) {
        temp_live_cell_buffer[i] = 0;
    }

    for (size_t i = 0; i < previous_total_nr_of_cells; ++i) {

        uint32_t state = grid->cells[i].state;
        
        uint16_t old_x = grid->cells[i].x_position;
        uint16_t old_y = grid->cells[i].y_position;

        if (state & IS_ALIVE) {
            if (old_x < grid->nr_of_cells_per_row && old_y < grid->nr_of_cells_per_column) {
                size_t new_cell_index = old_y*grid->nr_of_cells_per_row + old_x;
                Assert(new_cell_index < grid->total_nr_of_cells);
                temp_live_cell_buffer[new_cell_index] = &grid->cells[new_cell_index];
            }
        }
    }

    for (int i = 0; i < grid->total_nr_of_cells; ++i) {

        uint32_t state = 0;

        if (i % grid->nr_of_cells_per_row == 0) {
            state |= IS_IN_LEFT_COLUMN;
        }

        if (i % grid->nr_of_cells_per_row == grid->nr_of_cells_per_row - 1) {
            state |= IS_IN_RIGHT_COLUMN;
        }

        if (i < grid->nr_of_cells_per_row) {
            state |= IS_IN_TOP_ROW;
        }

        if (i + grid->nr_of_cells_per_row > (grid->total_nr_of_cells-1)) {
            state |= IS_IN_BOTTOM_ROW;
        }

        grid->cells[i].x_position = i % grid->nr_of_cells_per_row;
        grid->cells[i].y_position = i / grid->nr_of_cells_per_row;
        grid->cells[i].state = state;
        grid->cells[i].nr_of_live_neighbors = 0; 

        // Initialize all neighbors to zero. We initialize them to point to the proper cells in a second pass,
        // after we've initialized all of the cells.
        for (int j = 0; j < MAX_NR_OF_NEIGHBORS; ++j) {
            grid->cells[i].neighbors[j] = 0;
        }
    }

    for (int i = 0; i < grid->total_nr_of_cells; ++i) {
        if (temp_live_cell_buffer[i]) {
            temp_live_cell_buffer[i]->state |= IS_ALIVE;
        }
    }

    // Set the pointers to the neighbor cells to the correct addresses.
    for (int i = 0; i < grid->total_nr_of_cells; ++i) {
        cell **neighbors = grid->cells[i].neighbors;
        uint32_t current_cell_state = grid->cells[i].state;

        Bool has_neighbors_to_the_top    = !(current_cell_state & IS_IN_TOP_ROW);
        Bool has_neighbors_to_the_left   = !(current_cell_state & IS_IN_LEFT_COLUMN);
        Bool has_neighbors_to_the_right  = !(current_cell_state & IS_IN_RIGHT_COLUMN);
        Bool has_neighbors_to_the_bottom = !(current_cell_state & IS_IN_BOTTOM_ROW);

        if (has_neighbors_to_the_top) {
            Assert(i-grid->nr_of_cells_per_row >= 0);
            neighbors[TOP_MIDDLE_NEIGHBOR] = &grid->cells[i-grid->nr_of_cells_per_row];
            if (has_neighbors_to_the_left) {
                Assert((i-grid->nr_of_cells_per_row)-1 >= 0);
                neighbors[TOP_LEFT_NEIGHBOR] = &grid->cells[(i-grid->nr_of_cells_per_row)-1];
            }
            if (has_neighbors_to_the_right) {
                neighbors[TOP_RIGHT_NEIGHBOR] = &grid->cells[(i-grid->nr_of_cells_per_row)+1];
            }
        }

        if (has_neighbors_to_the_left) {
            Assert((i-1) >= 0);
            neighbors[LEFT_NEIGHBOR] = &grid->cells[i-1];
        }

        if (has_neighbors_to_the_right) {
            neighbors[RIGHT_NEIGHBOR] = &grid->cells[i+1];
        }

        if (has_neighbors_to_the_bottom) {
            Assert((i+grid->nr_of_cells_per_row) < grid->total_nr_of_cells);
            neighbors[BOTTOM_MIDDLE_NEIGHBOR] = &grid->cells[i+grid->nr_of_cells_per_row];
            if (has_neighbors_to_the_left) {
                neighbors[BOTTOM_LEFT_NEIGHBOR] = &grid->cells[(i+grid->nr_of_cells_per_row)-1];
            }
            if (has_neighbors_to_the_right) {
                Assert(i+grid->nr_of_cells_per_row+1 < grid->total_nr_of_cells);
                neighbors[BOTTOM_RIGHT_NEIGHBOR] = &grid->cells[(i+grid->nr_of_cells_per_row)+1];
            }
        }
    }
}

static void clear_grid_for_restart(grid *grid) {
    for (size_t i = 0; i < grid->total_nr_of_cells; ++i) {
        grid->cells[i].state &= (IS_IN_RIGHT_COLUMN|IS_IN_LEFT_COLUMN|IS_IN_TOP_ROW|IS_IN_BOTTOM_ROW);
    }
    grid->game_state = 0;
}

static void update_and_render_grid(gol_window_hdr hdr, grid grid, gol_window_dimensions dimensions) {

    // Condensed Game of Life rules:
    // 1. Any live cell with two or three live neighbors survives.
    // 2. Any dead cell with three live neighbors becomes a live cell.
    // 3. All other live cells die in the next generation. Similarly, all other dead cells stay dead.

    if (grid.game_state & GAME_STATE_RUNNING) {

        // Update the grid according to the rules of the game
        for (int i = 0; i < grid.total_nr_of_cells; ++i) {
            cell *current_cell = &grid.cells[i];
            int live_neighbors = 0;
            for (int j = 0; j < MAX_NR_OF_NEIGHBORS; ++j) {
                cell *neighbor = current_cell->neighbors[j];
                if (neighbor && (neighbor->state & IS_ALIVE)) {
                        ++live_neighbors;
                }
            }
            current_cell->nr_of_live_neighbors = live_neighbors;
        }

        for (int i = 0; i < grid.total_nr_of_cells; ++i) {
            cell *current_cell = &grid.cells[i];
            if (current_cell->state & IS_ALIVE) {
                if (current_cell->nr_of_live_neighbors < 2 || current_cell->nr_of_live_neighbors > 3) {
                    current_cell->state &= (~IS_ALIVE);
                }
             } else {
                if (current_cell->nr_of_live_neighbors == 3) {
                     current_cell->state |= IS_ALIVE;
                }
            }
        }
    }

    // Render the grid
    uint8_t *p = hdr.image_buffer;
    uint32_t current_cell_row = 0;
    uint32_t current_cell_column = 0;
    uint32_t current_cell_nr = 0;
    cell current_cell = grid.cells[0];
    for (size_t i = 0; i < dimensions.height; ++i) {
        if (i % SQUARE_SIZE_OF_CELL_IN_PIXELS == 0 && i > 0) { // completed a row
            ++current_cell_row;
            current_cell_nr = current_cell_row*grid.nr_of_cells_per_row + current_cell_column;
            current_cell = grid.cells[current_cell_nr];
        }
        for (size_t j = 0; j < dimensions.width; ++j) {
            if (j % SQUARE_SIZE_OF_CELL_IN_PIXELS == 0 && j > 0) { // completed a column
                ++current_cell_column;
                current_cell_nr = current_cell_row*grid.nr_of_cells_per_row + current_cell_column;
                current_cell = grid.cells[current_cell_nr];
            }
            if (i > grid.nr_of_cells_per_column*SQUARE_SIZE_OF_CELL_IN_PIXELS || // Wasted pixels at the bottom
                    j > grid.nr_of_cells_per_row*SQUARE_SIZE_OF_CELL_IN_PIXELS || // Wasted pixels at the right
                    i % SQUARE_SIZE_OF_CELL_IN_PIXELS == 0 || // Vertical inner border
                    j % SQUARE_SIZE_OF_CELL_IN_PIXELS == 0) { // Horizontal inner border

                *p++ = border_color.blue;
                *p++ = border_color.green;
                *p++ = border_color.red;
            } else if (current_cell.state & IS_ALIVE) {
                *p++ = live_cell_color.blue;
                *p++ = live_cell_color.green;
                *p++ = live_cell_color.red;
            } else {
                *p++ = dead_cell_color.blue;
                *p++ = dead_cell_color.green;
                *p++ = dead_cell_color.red;
            }
            *p++ = 0x0; // padding
        }
        current_cell_column = 0;
        current_cell_nr = current_cell_row*grid.nr_of_cells_per_row;
        current_cell = grid.cells[current_cell_nr];
    }

    // @incomplete(ltoorn): We should capture the `xcb_void_cookie_t` returned by `xcb_put_image()`
    // and use it to check for errors.
    xcb_put_image(
            hdr.connection,
            XCB_IMAGE_FORMAT_Z_PIXMAP,
            hdr.window_id,
            hdr.graphics_context_id,
            dimensions.width,
            dimensions.height,
            0, // dest_x, relative to the drawable's origin
            0, // dest_y, relative to the drawable's origin
            0, // Must be 0 for ZFormat pixmaps.
            hdr.screen->root_depth,
            (dimensions.width * dimensions.height * BYTES_PER_PIXEL), // data_len
            hdr.image_buffer); // *data
}

static cell *get_cell_at_coordinate(grid grid, coordinate2 cdt) {

    // Note that we expect the caller to perform bounds checking before translating the pixels to coordinates.
    // We can't do it here because we need to correct for the outer borders while translating the pixels to 
    // cell coordinates, and it's also not the case that pixel coordinates that are out of bounds will always 
    // lead to a cell index that is actually out of bounds for the cell buffer, since they can be out of bounds
    // relative to the row border.
    Assert(cdt.x < grid.nr_of_cells_per_row);
    Assert(cdt.y < grid.nr_of_cells_per_column);

    size_t cell_index = cdt.y*grid.nr_of_cells_per_row + cdt.x;
    cell *cell = &grid.cells[cell_index];
    
    return cell;
}

static void toggle_cells(grid grid, coordinate2 start_pixel_coordinate, coordinate2 end_pixel_coordinate) {

    Bool start_pixel_coordinate_is_in_bounds = IsPixelCoordinateInBounds(grid, start_pixel_coordinate);
    Bool end_pixel_coordinate_is_in_bounds = IsPixelCoordinateInBounds(grid, end_pixel_coordinate);
    
    if (start_pixel_coordinate_is_in_bounds && end_pixel_coordinate_is_in_bounds) {

        coordinate2 start;
        start.x = pixels_to_nr_of_cells(ClampNegativeToZero(start_pixel_coordinate.x));
        start.y = pixels_to_nr_of_cells(ClampNegativeToZero(start_pixel_coordinate.y));

        coordinate2 end;
        end.x = pixels_to_nr_of_cells(ClampNegativeToZero(end_pixel_coordinate.x));
        end.y = pixels_to_nr_of_cells(ClampNegativeToZero(end_pixel_coordinate.y));

        cell *cell_at_coordinate = get_cell_at_coordinate(grid, start);

        cell_at_coordinate->state ^= IS_ALIVE;
        while (cell_at_coordinate && (start.x != end.x || start.y != end.y)) {
            if (start.x < end.x) {
                ++start.x;
            } else if (start.x > end.x) {
                --start.x;
            }

            if (start.y < end.y) {
                ++start.y;
            } else if (start.y > end.y) {
                --start.y;
            }

            cell_at_coordinate = get_cell_at_coordinate(grid, start);
            cell_at_coordinate->state ^= IS_ALIVE;
        }
    }
}

int main(int argc, char **argv) {

    // Initializations

    gol_window_hdr hdr = initialize_window();
    grid grid = initialize_grid(hdr.root_dimensions);
    initialize_standard_colors();

    initialize_keycode_to_keysyms_lookup_table(hdr);

    // End initializations
 
    // A time spec used in `nanosleep()` to control the speed of the main loop (and thus the speed with which the generations
    // of cells live and die). The user can change these values within hardcoded bounds.
    struct timespec sleep_time_spec;
    sleep_time_spec.tv_sec = 0;
    sleep_time_spec.tv_nsec = TIME_SPEC_INITIAL_SIZE_NANO;

    // Event pointer to hold the events we'll receive in the event loop.
    xcb_generic_event_t *event;

    // @feedback(ltoorn): The x.org xcb tutorial contains a wrong signature for the poll_event function.
    // It lists the following signature: `xcb_poll_for_event(xcb_connection_t *c, int *error)`, but
    // the actual signature is as follows: `xcb_poll_for_event(xcb_connection_t *c)`.
    //
    // See: https://www.x.org/releases/current/doc/libxcb/tutorial/index.html#intro

    // Window dimensions struct to hold the dimensions of the window as it actually is presented on the screen.
    // This can change between exposure events if the window gets resized by the user (or the window manager).
    gol_window_dimensions dimensions = { 0 };
    coordinate2 button_press_pixel_coordinate = { -1, -1 };
    coordinate2 button_release_pixel_coordinate = { -1, -1 };

    DebugPrintLnNoArgs("entering event loop");
    for (;;) { 

        event = xcb_poll_for_event(hdr.connection);
        if (event) {

            // @todo(ltoorn): Handle some kind of quit event.
            switch (event->response_type & ~0x80) {
               
                case XCB_EXPOSE:
                    {
                        // We need to get the geometry of the actual window we can draw to, which can change on 
                        // each exposure event as the window gets (re)sized.
                        //
                        // @todo(ltoorn): error handling.
                        xcb_generic_error_t      *xcb_generic_error = 0;
                        xcb_get_geometry_cookie_t geometry_cookie   = xcb_get_geometry(hdr.connection, hdr.window_id);
                        xcb_get_geometry_reply_t *geometry_reply    = xcb_get_geometry_reply(
                                hdr.connection,
                                geometry_cookie,
                                &xcb_generic_error);

                        if (!geometry_reply) {
                            // @todo(ltoorn): error handling.
                            // This shouldn't be a fatal error. We should just draw to some default dimensions, either
                            // those of the root window (which would be equivalent to drawing full screen), or some
                            // arbitrary size that would fit decently on a standard 13" laptop screen.
                            handle_fatal_error("unable to retrieve window dimensions");
                        }
 
                        dimensions.width  = geometry_reply->width;
                        dimensions.height = geometry_reply->height; 

                        adjust_visible_grid_size_to_window_dimensions(&grid, dimensions);

                        DebugPrintLnNoArgs("XCB_EXPOSE event");
                    } 
                    break;
               
                case XCB_KEY_PRESS: 
                    {
                        xcb_key_press_event_t *key_press_event = (xcb_key_press_event_t *) event;

                        xcb_keysym_t keysym = translate_keycode_to_keysym(key_press_event->detail);

                        if (IsFunctionKey(keysym)) {
                            DebugPrintLnNoArgs("FUNCTION KEY");
                            DebugPrintLnArgs("keysym hex: 0x%x", keysym);

                            if (keysym == DOWN_ARROW_KEYSYM) {
                                if (sleep_time_spec.tv_nsec < UPPER_BOUND_TIME_SPEC_NANO) {
                                    sleep_time_spec.tv_nsec += TIME_SPEC_STEP_SIZE_NANO;
                                } else if (sleep_time_spec.tv_sec < UPPER_BOUND_TIME_SPEC_SEC) {
                                    sleep_time_spec.tv_sec += TIME_SPEC_STEP_SIZE_SEC;
                                    sleep_time_spec.tv_nsec = 0;
                                }
                            } else if (keysym == UP_ARROW_KEYSYM) {
                                if (sleep_time_spec.tv_nsec > LOWER_BOUND_TIME_SPEC_NANO) {
                                    sleep_time_spec.tv_nsec -= TIME_SPEC_STEP_SIZE_NANO;
                                } else if (sleep_time_spec.tv_sec > LOWER_BOUND_TIME_SPEC_SEC) {
                                    sleep_time_spec.tv_sec -= TIME_SPEC_STEP_SIZE_SEC;
                                    sleep_time_spec.tv_nsec = UPPER_BOUND_TIME_SPEC_NANO;
                                }
                            }
                        } else {
                            uint32_t code_point = keysym;

                            DebugPrintLnArgs("\ncode_point: %u -- %c", code_point, code_point);
                            if (code_point == 'q') {
                                return 0;
                            } else if (code_point == ' ') {
                                if (grid.game_state & GAME_STATE_RUNNING) {
                                    grid.game_state ^= GAME_STATE_PAUSED;
                                } else {
                                    grid.game_state |= GAME_STATE_RUNNING;
                                }
                            } else if (code_point == 'r') {
                                clear_grid_for_restart(&grid);
                                update_and_render_grid(hdr, grid, dimensions);
                            }
                        }
                        DebugPrintLnArgs("sleep_time_spec.tv_sec: %ld, sleep_time_spec.tv_nsec: %lu", sleep_time_spec.tv_sec, sleep_time_spec.tv_nsec);
                    }
                    break;
 
                case XCB_BUTTON_PRESS:
                    {
                        xcb_button_press_event_t *button_press_event = (xcb_button_press_event_t *) event;

                        button_press_pixel_coordinate.x = button_press_event->event_x;
                        button_press_pixel_coordinate.y = button_press_event->event_y;
                        DebugPrintLnArgs("button press -- event_x: %d, event_y: %d", button_press_pixel_coordinate.x, button_press_pixel_coordinate.y);
                    }
                    DebugPrintLnNoArgs("XCB_BUTTON_PRESS event");
                    break;

                case XCB_BUTTON_RELEASE:
                    {
                        xcb_button_release_event_t *button_release_event = (xcb_button_press_event_t *) event;
                        button_release_pixel_coordinate.x = button_release_event->event_x;
                        button_release_pixel_coordinate.y = button_release_event->event_y;

                        toggle_cells(grid, button_press_pixel_coordinate, button_release_pixel_coordinate);

                        DebugPrintLnArgs("button release -- event_x: %d, event_y: %d", button_release_pixel_coordinate.x, button_release_pixel_coordinate.y);
                    }
                    DebugPrintLnNoArgs("XCB_BUTTON_RELEASE event");
                    break;
                
                default:
                    DebugPrintLnNoArgs("default");
                    break;
            }
            free(event);
        } else {
            if (!(grid.game_state & GAME_STATE_PAUSED)) {
                if (grid.game_state & GAME_STATE_RUNNING) {
                    nanosleep(&sleep_time_spec, 0x0);
                }
                update_and_render_grid(hdr, grid, dimensions);
            }
        }
    }

    return 0;
}

